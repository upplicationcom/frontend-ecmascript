/**
 * Set of helpers for assisting the tests assertions
 */

/**
 * Asserts that the given object is a valid {@link Promise} (has then and catch)
 * @param {*} obj - The object to test if is an {@link Promise} or not.
 * @returns {boolean} true always (if no AssertionError was thrown).
 * @throws {AssertionError} If the provided argument is not a valid {@link Promise}
 */
export function isPromise(obj) {
    expect(obj).toBeDefined()
    expect(typeof obj.then).toBe('function')
    expect(typeof obj.catch).toBe('function')
    return true
}

/**
 * Asserts that the given object is a valid {@link Application}.
 * @param {*} obj - The object to test if is an {@link Application} or not.
 * @returns {boolean} true always (if no AssertionError was thrown).
 * @throws {AssertionError} If the provided argument is not a valid {@link Application}
 */
export function isApplication(obj) {
    expect(obj).toBeDefined()
    expect(typeof obj.id).toBe('number')
    expect(typeof obj.name).toBe('string')
    expect(obj.name.length).toBeGreaterThan(0)
    return true
}

/**
 * Asserts that the given object is a valid {@link PaginationInfo}.
 * @param {*} obj - The object to test if is an {@link PaginationInfo} or not.
 * @returns {boolean} true always (if no AssertionError was thrown).
 * @throws {AssertionError} If the provided argument is not a valid {@link PaginationInfo}
 */
export function isPaginationInfo(obj) {
    expect(obj).toBeDefined()
    expect(typeof obj.start).toBe('number')
    expect(typeof obj.limit).toBe('number')
    expect(typeof obj.total).toBe('number')
    expect(obj.start).toBeGreaterThanOrEqual(0)
    expect(obj.limit).toBeGreaterThanOrEqual(0)
    expect(obj.total).toBeGreaterThanOrEqual(0)
    return true
}

/**
 * Asserts that the given object correctly implements the {@Paginated} interface.
 * @param {*} obj - The object to test if implements {@link Paginated} or not.
 * @returns {boolean} true always (if no AssertionError was thrown).
 * @throws {AssertionError} If the provided argument does not implement {@link Paginated}
 */
export function implementsPaginated(obj) {
    expect(obj).toBeDefined()
    expect(isPaginationInfo(obj.pagination)).toBeTrue()
    return true
}
