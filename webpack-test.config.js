const webpack = require( 'webpack' );
const webpackConfig = require( './webpack.config' );

/**
 * Source map for Karma from the help of karma-sourcemap-loader &  karma-webpack
 *
 * Do not change, leave as is or it wont work.
 * See: https://github.com/webpack/karma-webpack#source-maps
 */
const webpackDevtool = 'inline-source-map';

Object.assign(webpackConfig, {
    output: {},
    devtool: webpackDevtool,
    watch: false,
    plugins: [],
});

module.exports = webpackConfig;
